'use strict'

/*
 *  Modify this file as needed.
 */

const http = require('http');

process.on('SIGTERM', function () {
    process.exit(0)
})


/**
 *
 * Server listens to incoming requests and it determines dynamically how to process it.
 * Decision is made based on headers. Currently it supports only `gzip` and plain JSON, however it's easy to add new
 * data format like Protobuf etc.
 *
 */
const server = http.createServer(function (req, res) {
    const headers = req.headers;

    // If request is gzip encoded
    if (headers['content-encoding'] === 'gzip') {
        return require('./components/zlib-request-handler')(req, res);
    }

    // If it's plain JSON
    if (headers['content-type'] === 'json') {
        return require('./components/json-request-handler')(req, res);
    }

    // Throw an error to indicate that data format is not supported yet.
    throw new Error('Unsupported data format.');
})

server.listen(8080)
