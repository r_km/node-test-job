'use strict';

const zlib = require('zlib');

module.exports = (req, res) => {
    const gunzip = zlib.createGunzip();
    req.pipe(gunzip);

    let message = '';
    gunzip.on('data', data => {
        message += data;
    });
    gunzip.on('end', () => {
        console.log(message);
        res.end();
    });
};