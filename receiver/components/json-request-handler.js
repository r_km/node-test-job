'use strict';

module.exports = (req, res) => {
    let body = [];

    req.on('data', data => body.push.bind(body));

    req.on('end', () => {
        console.log(Buffer.concat(body).toString());
        res.end();
    });
};