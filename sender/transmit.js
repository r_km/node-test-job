'use strict';

const SensorData = require('./components/sensor-data');
const SensorDataService = require('./components/sensor-data-service');
const DataSender = require('./components/sensor-data-sender');
const GzipCompressStrategy = require('./components/strategies/gzip-compress-strategy');

// Compressing strategy can be passed dynamically or can be changed later via appropriate setter, so one can create his
// own strategy and use it, or use some already defined formats like protobuf.
const sensorDataService = new SensorDataService(new GzipCompressStrategy()),
    dataSender = new DataSender();

/*
 *  This function will be called for each event.  (eg: for each sensor reading)
 *  Modify it as needed.
 */
module.exports = function (eventMsg, encoding, callback) {
    const sensorData = new SensorData(eventMsg);

    sensorDataService
        .compress(sensorData.rawData)
        .then(compressedDataObj => {
            sensorData.compressedData = compressedDataObj.data;
            sensorData.meta = compressedDataObj.meta;
            return dataSender.addItem(sensorData);
        })
        .then(() => {
            callback();
        })
        .catch(err => {
            console.log(`Error during sending data from sensors`);
            console.log(err);
            callback();
        });
}
