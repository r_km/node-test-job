'use strict';

const chai = require('chai');
chai.should();

const SensorData = require('./sensor-data');

describe('SensorData:', () => {
    it ('Should create SensorData object', done => {
        const sensorData = new SensorData({});
        (sensorData instanceof SensorData).should.be.true;
        done();
    });

    it ('Should set sensor data during object creation', done => {
        const sensorData = new SensorData('12345test');
        sensorData.rawData.should.equal('12345test');
        done();
    });

    it ('Should not set and sensor data after object creation', done => {
        const sensorData = new SensorData(123);
        sensorData.rawData.should.equal(123);
        try {
            sensorData.rawData = 'test';
        } catch (e) {
            (e instanceof Error).should.be.true;
            sensorData.rawData.should.equal(123);
            done();
        }

    });

    it ('Should set and get `meta` object dynamically', done => {
        const sensorData = new SensorData(123);
        // Initially it should be empty
        (sensorData.meta === null).should.be.true;

        sensorData.meta = { a: 1 };
        sensorData.meta.a.should.equal(1);
        done();
    });

    it ('Should set and get `compressedData` object dynamically', done => {
        const sensorData = new SensorData(123);
        // Initially it should be empty
        (sensorData.compressedData === null).should.be.true;

        sensorData.compressedData = { a: 1 };
        sensorData.compressedData.a.should.equal(1);
        done();
    });
});