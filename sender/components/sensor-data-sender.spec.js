'use strict';

const chai = require('chai');
chai.should();

const SensorDataSender = require('./sensor-data-sender');

describe('SensorDataSender:', () => {
    it ('Should create instance: ', done => {
        const sender = new SensorDataSender();
        (sender instanceof SensorDataSender).should.be.true;
        done();
    });

    it ('Fetch should return null if there are no items: ', done => {
        const sender = new SensorDataSender();
        (sender.fetchItem() === null).should.be.true;
        done();
    });
});