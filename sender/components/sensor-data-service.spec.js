'use strict';

const chai = require('chai');
chai.should();

const SensorDataService = require('./sensor-data-service');
const BaseCompressStrategy = require('./strategies/base-compress-strategy');
const GzipCompressStrategy = require('./strategies/gzip-compress-strategy');

describe('SensorDataService:', () => {
    it ('Should create new object correctly with default BaseCompressStrategy', done => {
        const sensorDataService = new SensorDataService();
        (sensorDataService instanceof SensorDataService).should.be.true;
        (sensorDataService.compressStrategy instanceof  BaseCompressStrategy).should.be.true;
        (typeof sensorDataService.compress === 'function').should.be.true;
        done();
    });

    it ('Should throw error if strategy does not meet requirements', done => {
        try {
            new SensorDataService({});
        } catch (e) {
            (e instanceof Error).should.be.true;
            done();
        }
    });

    it ('Should be created with custom strategy', done => {
        const sensorDataService = new SensorDataService(new GzipCompressStrategy());
        (sensorDataService.compressStrategy instanceof GzipCompressStrategy).should.be.true;
        done();
    });

    it ('Should change strategy in runtime', done => {
        const sensorDataService = new SensorDataService(new GzipCompressStrategy());
        (sensorDataService.compressStrategy instanceof GzipCompressStrategy).should.be.true;

        sensorDataService.compressStrategy = new BaseCompressStrategy();
        (sensorDataService.compressStrategy instanceof BaseCompressStrategy).should.be.true;
        done();
    });

    it ('Should not accept malformed strategy in runtime', done => {
        const sensorDataService = new SensorDataService(new GzipCompressStrategy());
        (sensorDataService.compressStrategy instanceof GzipCompressStrategy).should.be.true;

        try {
            sensorDataService.compressStrategy = {};
        } catch (e) {
            (e instanceof Error).should.be.true;
            done();
        }
    });
});