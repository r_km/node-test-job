'use strict';

/**
 * Encapsulates sensor's data and keeps compressed data with all meta information about the compression method etc
 */
class SensorData {
    constructor (rawData) {
        /**
         * Keeps raw data received from the sensors.
         * @type {{Object}}
         * @private
         */
        this._rawData = rawData;
        /**
         * Metadata for compressed data. This is needed because here we don't know which compression strategy would be used
         * but we need to know how to send data through HTTP POST.
         *
         * @type {{Object}}
         * @private
         */
        this._meta = null;

        /**
         * Keeps compressed data.
         * @type {null}
         * @private
         */
        this._compressedData = null;
    }

    get rawData () {
        return this._rawData;
    }

    get compressedData () {
        return this._compressedData;
    }

    set compressedData (data) {
        this._compressedData = data;
    }

    get meta () {
        return this._meta;
    }

    set meta (meta) {
        this._meta = meta;
    }


}

module.exports = SensorData;