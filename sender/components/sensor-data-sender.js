'use strict';

const request = require('request-promise-native');

/**
 * This is needed to stop timer from checking items. If there are no items for 10 times one by one
 * then we can assume that stream has already been finished.
 *
 * @type {number}
 */
const NO_ENTRIES_MAX_COUNT = 10;

/**
 * Initially we didn't find no data in the list.
 *
 * @type {number}
 */
let noEntriesFoundCount = 0;

/**
 * This class is responsible for sending data to the server.
 * Basic idea is to send data smoothly in order to provide the most efficient way to deliver data with minimum loses.
 *
 * Data from sensors is kept into internal storage and sending it to the server is performed by this class.
 */
class SensorDataSender {
    constructor () {
        /**
         * Internal storage for data from the sensors. It's needed to provide smooth transition of each package one by one.
         * By package I mean a portion of data received from the sensor in one cycle.
         *
         * @type {Array}
         * @private
         */
        this._items = [];

        // Keeps reference to running interval.
        this._timeout = null;
        // Determines whether there is request in progress.
        this._isRequestInProgress = false;
    }

    /**
     * Starts interval that runs handler for items stack.
     */
    initTimer () {
        this._timeout = setInterval(this.handleItem.bind(this), 500);
    }

    /**
     * Method responsible for processing data from sensor.
     */
    handleItem () {
        const item = this.fetchItem();
        if (item) {
            noEntriesFoundCount = 0;
            this.sendData(item);
        } else {
            noEntriesFoundCount++;
        }

        // Basically if methods cannot find any new data `NO_ENTRIES_MAX_COUNT` times one by one, then
        // we can assume that there is no more data.
        // This is also needed in order to get `test.sh` script working
        if (noEntriesFoundCount > NO_ENTRIES_MAX_COUNT) {
            console.log('Clearing interval as there are no more entries.');
            clearInterval(this._timeout);
            this._timeout = null;
        }
    }

    /**
     * Takes portion of data from the stack and returns it.
     * @returns {*}
     */
    fetchItem () {
        if (this._items.length > 0) {
            return this._items.shift();
        }
        return null;
    }

    /**
     * Adds item to the stack of items
     *
     * @param item
     */
    addItem (item) {
        // Otherwise add item into the queue.
        this._items.push(item);

        // If there is no handler already scheduled and if there is no running request - then we can send data to the server.
        if (!this._timeout && !this._isRequestInProgress) {
            this.handleItem();
        }

        // Set timer to check stack with data
        if (!this._timeout) {
            this.initTimer();
        }
    }

    /**
     * Sends compressed sensor's data to the server.
     * Sets appropriate headers based on selected compression algorithm.
     *
     * @param sensorData
     * @returns {Promise}
     */
    sendData (sensorData) {
        this._isRequestInProgress = true;
        const options = {
            uri: 'http://localhost:8080/event',
            method: 'POST',
            body: sensorData.compressedData,
            json: sensorData.meta.isJSON,
            headers: sensorData.meta.httpHeaders
        };

        return request(options)
            .then(() => {
                this._isRequestInProgress = false;
            })
            .catch(err => {
                this._isRequestInProgress = false;
                throw err;
            })
    }
}

module.exports = SensorDataSender;