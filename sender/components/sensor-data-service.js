'use strict';

const BaseCompressStrategy = require('./strategies/base-compress-strategy');

/**
 * The purpose of this class is to encapsulate algorithm for compressing data received from the sensors.
 *
 * It allows to choose method for encoding JSON. It's implemented via Strategy pattern and it makes it more flexible,
 * especially in case of adding new encoding strategy in future or changing it in runtime.
 *
 */

class SensorDataService {

    constructor (compressStrategy = new BaseCompressStrategy()) {
        // Ensure that strategy is an object and has `compress` method.
        if (typeof compressStrategy !== 'object' || typeof compressStrategy.compress !== 'function') {
            throw new Error('Strategy should be an object and should implement `compress` method!');
        }

        this._compressStrategy = compressStrategy;
    }


    /**
     * Compress raw sensor data via injected compress strategy.
     * Assuming that raw data from the sensors will always be in JSON.
     *
     * @param rawSensorData JSON with data from sensors.
     */
    compress (rawSensorData) {
        return this._compressStrategy.compress(rawSensorData);
    }

    get compressStrategy () {
        return this._compressStrategy;
    }

    set compressStrategy (compressStrategy) {
        // Ensure that strategy is an object and has `compress` method.
        if (typeof compressStrategy !== 'object' || typeof compressStrategy.compress !== 'function') {
            throw new Error('Strategy should be an object and should implement `compress` method!');
        }

        this._compressStrategy = compressStrategy;
    }

}

module.exports = SensorDataService;