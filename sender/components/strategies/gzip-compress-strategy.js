'use strict';

const zlib = require('zlib');

/**
 * Implements gzip compression.
 */
class GzipCompressStrategy {

    compress (data) {
        return new Promise ((resolve, reject) => {
            zlib.gzip(JSON.stringify(data), (err, buffer) => {
                if (err) {
                    console.log(`Error during gzip compressing: `, err);
                    return reject(err);
                }
                resolve({
                    data: buffer,
                    meta: {
                        httpHeaders: {
                            "Content-Encoding": 'gzip'
                        },
                        isJSON: false
                    }
                });
            });

        });
    }
}

module.exports = GzipCompressStrategy;