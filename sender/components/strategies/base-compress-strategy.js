'use strict';


/**
 * This class implements Base Compress Strategy that actually does nothing - it returns plain JSON with appropriate headers.
 * However it's needed to keep inheritance tree and allow polymorphism.
 */

class BaseCompressStrategy {
    /**
     * It does nothing, just treats input data as a JSON and returns appropriate HTTP headers.
     *
     * @param data
     */
    compress (data = {}) {
        // Return Promise for compatibility with async strategies.
        return Promise.resolve({
            data,
            meta: {
                httpHeaders: {
                    "Content-Type": 'application/json'
                }
            }
        });
    }
}

module.exports = BaseCompressStrategy;